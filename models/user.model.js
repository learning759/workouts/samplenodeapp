const { default: mongoose } = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    name: {
      firstName: {
        maxlength: 50,
        type: String,
        trim: true,
        required: true,
      },
      lastName: {
        maxlength: 50,
        type: String,
        trim: true,
        required: true,
      },
    },
    password: {
      type: String,
      trim: true,
      required: true,
    },
    email: {
      type: String,
      lowercase: true,
      trim: true,
      required: true,
    },
    activity: {
      lastLogin: {
        type: Date,
        default: Date.now(),
      },
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("user", userSchema);
