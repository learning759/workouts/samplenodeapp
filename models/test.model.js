const { default: mongoose } = require("mongoose");
const { getLocalTime } = require("../shared/utils.shared");
const { STATUS } = require("../shared/constants.shared");

const userSchema = new mongoose.Schema(
  {
    name: {
      firstName: {
        maxlength: 50,
        type: String,
        required: true,
        trim: true,
      },
      lastName: {
        maxlength: 50,
        type: String,
        required: true,
        trim: true,
      },
    },
    createdBy: {
      type: String,
      default: "user",
      enum: ["user", "admin"],
    },
    updatedBy: {
      //to identify the  updated person
      type: String,
      trim: true,
    },
    dob: {
      //date of birth will be stored
      type: Date,
    },

    profileImg: {
      Type: String,
      default: "",
    },
    contactDetails: {
      email: {
        //email of the user it will be unique
        type: String,
        lowercase: true,
        trim: true,
        required: true,
      },
      mobileNo: {
        //mobileNo of the user it will be unique
        type: Number,
        required: true,
      },
      country: {
        //state of the user
        type: String,
        trim: true,
      },
      city: {
        type: String,
        trim: true,
      },
    },
    activity: {
      isVerified: {
        email: {
          type: Boolean,
          default: false,
        },

        mobileNo: {
          type: Boolean,
          default: false,
        },
      },
      isLocked: {
        //to block the user from actvities if find any fraud activity
        type: Boolean,
        default: false,
        enum: [true, false],
      },
      WrongAttempts: {
        // for storing the incorrect password
        type: Number,
        default: 0,
      },
      status: {
        type: String,
        default: STATUS.ACTIVE,
        enum: [STATUS.ACTIVE, STATUS.BLOCKED, STATUS.DELETE],
      },
      lastLogin: {
        //to update the last login time by the user
        type: Date,
        default: getLocalTime(),
      },
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("test", userSchema);
