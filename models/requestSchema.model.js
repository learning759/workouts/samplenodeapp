const { PATTERNS } = require("../shared/constants.shared");

const dobModel = {
  type: "string",
  format: "dd mmm yyyy",
};
const emailModel = {
  type: "string",
  format: "email",
  pattern: PATTERNS.EMAIL,
};
const nameModel = {
  type: "string",
  maxLength: 50,
  minLength: 3,
  // pattern: PATTERNS.ALPHABET,
};

const addressModel = {
  type: "string",
  minLength: 3,
  maxLength: 150,
};
const mobileNoModel = {
  type: "string",
  pattern: PATTERNS.NUMBER,
  minLength: 5,
};

exports.userCreateSchema = {
  properties: {
    firstName: nameModel,
    lastName: nameModel,
    password: { type: "string" },
    email: emailModel,
  },
  required: ["firstName", "lastName", "email", "password"],
};

exports.loginSchema = {
  properties: {
    email: emailModel,
    password: { type: "string" },
  },
  required: ["email", "password"],
};
