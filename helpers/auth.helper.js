const { fetchUser_Email } = require("../middleware/fetchDb.middleware");
const { createUser } = require("../middleware/writeDb.middleware");
const { STATUS_CODE_MSG } = require("../shared/constants.shared");

const isEmailExist = async (email) => {
  const [err, succ] = await fetchUser_Email(email);
  return err || !succ ? false : true;
};

const isUserExist = async ({ email }) => {
  try {
    const isExist = await isEmailExist(email);
    if (isExist) throw "Email already Exists";
    return false;
  } catch (error) {
    return error;
  }
};

const regUserHelper = async ({ firstName, lastName, password, email }) => {
  try {
    const [db_err, db_succ] = await createUser({
      name: {
        firstName,
        lastName,
      },
      password,
      email,
    });

    if (db_err) {
      console.log(db_err);
      throw STATUS_CODE_MSG.CREATION_FAILED;
    }

    return false;
  } catch (error) {
    return error;
  }
};

const newLogiTokenCreation = (user) => {
  const payload = {
    _id: user._id,
    email: user?.email || "",
    userName: user?.userName || user?.firstName || "",
  };
};

module.exports = {
  isUserExist,
  regUserHelper,
  newLogiTokenCreation,
};
