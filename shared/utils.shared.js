const bcrypt = require("bcryptjs");
const { to } = require("await-to-js");
const pe = require("parse-error");

//to get the local time based on the timezone
const getLocalTime = (date = new Date(), timeZone = "Asia/Calcutta") => {
  return new Date(date).toLocaleString("en-US", {
    timeZone: timeZone,
  });
};

// for returning the formatValidationError on api request
const formatValidationError = (errorArray) => {
  let errorMessage = `Required `;
  const a = errorArray[0].property.split("instance.")[1];
  errorMessage = `${errorArray[0]?.stack || errorArray[0]?.message}`;
  return errorMessage;
};

const encryptPassword = async (password) => await bcrypt.hashSync(password, 10);

const verifyPassword = async (password, hash) =>
  await bcrypt.compareSync(password, hash);

const solvePromise = async (promise) => {
  /*to solve the promise functions asynchronous and return the success and error in array format*/
  let err, res;
  [err, res] = await to(promise);
  if (err) return [pe(err)];

  return [null, res];
};

module.exports = {
  getLocalTime,
  formatValidationError,
  encryptPassword,
  solvePromise,
  verifyPassword,
};
