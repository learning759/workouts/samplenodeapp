const express = require("express");
const {
  BodyValidate_register,
  BodyValidate_login,
} = require("../middleware/requestValidator.middleware");
const {
  registerController,
  loginController,
} = require("../controller/auth.controller");
const app = express();

app.post("/register", BodyValidate_register, registerController);
app.post("/login", BodyValidate_login, loginController);

module.exports = app;
