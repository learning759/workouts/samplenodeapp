const { isUserExist, regUserHelper } = require("../helpers/auth.helper");
const { fetchUser_Email } = require("../middleware/fetchDb.middleware");
const { STRINGS } = require("../shared/constants.shared");
const { responseError, responseSucess } = require("../shared/response.shared");
const { encryptPassword, verifyPassword } = require("../shared/utils.shared");

const registerController = async (req, res) => {
  try {
    const { password, email } = req.body;

    const [isExist, encryptedPassword] = await Promise.all([
      isUserExist({ email }),
      encryptPassword(password),
    ]);

    if (isExist) throw "User already exists";

    const dbWrite = await regUserHelper({
      ...req.body,
      password: encryptedPassword,
    });
    if (dbWrite) throw dbWrite;
    responseSucess(res, STRINGS.ACCOUNT_REG);
  } catch (error) {
    console.log(`Error in register controller: ${JSON.stringify(error)}`);
    responseError(res, error);
  }
};

const loginController = async (req, res) => {
  try {
    const { email, password } = req.body;

    const [user_fetch_err, user_fetch_succ] = await fetchUser_Email(email);
    if (user_fetch_err) throw user_fetch_err;

    const { password: encryptedPassword } = user_fetch_succ;

    const verifyPswrd = await verifyPassword(password, encryptedPassword);

    if (!verifyPswrd) throw "Entered wrong credentials";

    // const token =
    responseSucess(res, STRINGS.LOGIN_SUCCESS, {
      user: {
        name: `${user_fetch_succ?.name?.firstName} ${user_fetch_succ?.name?.lastName}`,
        email: user_fetch_succ?.email || "",
      },
    });
  } catch (error) {
    console.log(`Error in loginController: ${JSON.stringify(error)}`);
    responseError(res, error);
  }
};

module.exports = {
  registerController,
  loginController,
};
