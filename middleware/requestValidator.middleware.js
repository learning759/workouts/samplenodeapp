const {
  userCreateSchema,
  loginSchema,
} = require("../models/requestSchema.model");
const { STATUS_CODE_MSG } = require("../shared/constants.shared");
const { responseError, mergeResponse } = require("../shared/response.shared");
const { formatValidationError } = require("../shared/utils.shared");

const Validator = require("jsonschema").Validator;
const valid = new Validator();

const validate = (validRes, res, next) => {
  if (!validRes.length) return next();

  return responseError(
    res,
    mergeResponse(
      STATUS_CODE_MSG.BAD_REQUEST,
      `${formatValidationError(validRes)}`
    )
  );
};

const validateBody = (req, res, next, model) =>
  validate(valid.validate(req.body, model).errors, res, next);

exports.BodyValidate_register = (req, res, next) =>
  validateBody(req, res, next, userCreateSchema);

exports.BodyValidate_login = (req, res, next) =>
  validateBody(req, res, next, loginSchema);
