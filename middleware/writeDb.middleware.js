const userModel = require("../models/user.model");
const { solvePromise } = require("../shared/utils.shared");

exports.createUser = (data) => solvePromise(new userModel(data).save());
