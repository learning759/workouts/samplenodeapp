const userModel = require("../models/user.model");
const { solvePromise } = require("../shared/utils.shared");

exports.fetchUser_Email = (email) =>
  solvePromise(
    userModel.findOne({
      email,
    })
  );
